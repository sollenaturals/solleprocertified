<?php
if($_POST['formSubmit'] == "Submit")
{
    $errorMessage = "";
	
	if(empty($_POST['formName']))
	{
		$errorMessage .= "<li>You forgot to enter a name!</li>";
	}
	if(empty($_POST['formEmail']))
	{
		$errorMessage .= "<li>You forgot to enter an email address!</li>";
	}
	
	$varName = $_POST['formName'];
	$varEmail = $_POST['formEmail'];

	if(empty($errorMessage)) 
	{
		$fs = fopen("123Tsv.csv","a");
		fwrite($fs,$varName . ", " . $varEmail . "\n");
		fclose($fs);
        $msg = $varName . '(' . $varEmail . ')' . ' is now SollePro Certified and needs their certificate';
        mail ('ledhead0501@gmail.com', 'SolleProCertified', $msg);
		header("Location: /solleprocertified/thankyou.html");
		exit;
	}
}
?>
<!DOCTYPE = html>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/a0d03b02a0.js"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="/solleprocertified/js/jquery.modal.min.js"></script>
<script src="/solleprocertified/js/jquery.js"></script>
<script src="/solleprocertified/js/quiz.js"></script>
<html>
<html lang = en>
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" >
<link href="https://fonts.googleapis.com/css?family=Oswald|Trocchi|Lobster|Montserrat|Merriweather|Shrikhand|Raleway|Montserrat|Creepster|Ribeye+Marrow|Luckiest+Guy" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,900,700italic,900italic,500,500italic, 300' rel='stylesheet' type='text/css'>
<link href="/solleprocertified/css/jquery.modal.css" type="text/css" rel="stylesheet" />
<link href="/solleprocertified/css/kustom.css" type="text/css" rel="stylesheet" />
<style>
  #top_border {
  list-style-type: none;
  margin: 0;
  padding : 0;
  overflow: hidden;
  position: fixed;
  background-color: #3d3d3d;
  border-radius: 0px;
  width: 100%;
  height: 50px;
  z-index: +1;
  margin-top: -70px;
  }
  
  
  li{
    display: block;
    color: white;
    float: right;
    padding: 15px 17px;
    font-family: Roboto;
    font-weight: bold;
    text-align: center;
    
  }
  li:hover{
      background-color: #c1d72e;
  }
  a{
      color: white;
  }
  a:hover{
      color: white;
      text-decoration: none;
  }
  a:click{
      color: white;
      text-decoration: none;
  }
 #mid_border{
 list-style-type:none;
 margin-top: -20px;
 padding: 0;
 overflow: hidden;
 position: fixed;
 background-color: #c1d72e;
 border-radius: 0px;
 width: 100%;
 height: 5px;
 box-shadow: 0px 3px 2px 0px #3b3b3b;
 }
 #img_logo{
  margin-left: 30px;
  margin-right: auto;
  margin-top: 5px;
  }
  
   .answers li {
margin-left: 30px;
list-style: upper-alpha;
color: white;
}

.answers2 li{
margin-left: 30px;
list-style: upper-alpha;
color: #3d3d3d;
}

.answers{
    display: block;
    margin-left: 7%;
    font-weight: bold;
    text-shadow: 1px 1px 1px #3d3d3d;
}

.answers2{
display: block;
margin-left: 7%;
font-weight: bold;
text-shadow: none;
color: #3d3d3d;
}



label {
margin-left: 0.5em;
cursor: pointer;
color: white
}

#altlabel{
color: #3d3d3d;
}

.question{
    color: white;
    font-weight: bold;
    margin-left: 5%;
    margin-top: 20px;
    text-shadow: 1px 1px 1px #3d3d3d;
}

.question2{
color: #3d3d3d;
font-weight: bold;
margin-left: 5%;
margin-top: 20px;
text-shadow: none;
}

#results {
background-color:rgba(0,0,0,0,0);
color: white;
padding: 3px;
margin-left: auto;
margin-right: auto;
margin-bottom: 25px;
border: 2px solid;
border-color: white;
text-align: center;
width: 200px;
cursor: pointer;
border-radius: 15px;
}

#results:hover {
background: #c1d72e;
color: white;
padding: 3px;
text-align: center;
width: 200px;
cursor: pointer;
border: 2px solid white;
}

#categorylist {
margin-top: 6px;
display: none;
color: white;
}

#category1, #category2, #category3, #category4, #category5, #category6, #category7, #category8, #category9, #category10, #category11 {
display: none;
color: white;
}

#closing {
display: none;
font-style: italic;
color: white;
} 

 
 body{
 background: radial-gradient(#929292, #424242); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
 }
 
 .bannerbar {
  border: 0px;
  border-style: solid;
  border-radius: 2px;
  padding-top: 0px;
  padding-bottom: 0px;
  margin: 0 auto;
  margin-top: 20px;
  margin-left: 0.5px;
  background: #c1d72e;
  max-width: 75%;
  box-shadow: 10px 5px 5px #3d3d3d;
  }
.bannerbar2{
  border: 0px;
  border-style: solid;
  border-radius: 2px;
  padding-top: 0px;
  padding-bottom: 0px;
  margin: 0 auto;
  margin-top: 20px;
  margin-right: 15%;
  background: #3d3d3d;
  max-width: 1024px;
  box-shadow: 10px 5px 5px #3d3d3d;
}
 
 .introslides{
     display: block;
     margin-left: auto;
     margin-right: auto;
     margin-top: 10px;
     margin-bottom: 20px;
     z-index: -1;
 }
 
 h1{
 font-family: Roboto;
 font-weight: bold;
 color: white;}
 
 .bodytext{
     color: white;
     font-family: Roboto;
     font-size: 25px;
     font-weight: bold;
     text-shadow: 1px 1px 1px #3d3d3d;
 }
 
 #productcertbutton{
     display: block;
     position: relative;
     font-family: Roboto;
     font-weight: bold;
     font-size: 20px;
     margin-left: auto;
     margin-right: auto;
     margin-bottom: 20px;
     margin-top: 30px;
     background-color:rgba(0,0,0,0.0);
     color: white;
     border-style: solid;
     border-color: white;
     border-radius: 10px;
 }
 
 #productcertbutton:hover{
     background-color: #c1d72e;
     color: white;
 }
 #mentorcertbutton{
     display: block;
     float: right;
     font-family: Roboto;
     font-weight: bold;
     font-size: 20px;
     margin-right: 20%;
     margin-bottom: 20px;
     background-color:rgba(0,0,0,0.0);
     color: white;
     border-style: solid;
     border-color: white;
     border-radius: 10px;
 }
 
 #mentorcertbutton:hover{
     background-color: #009ec5;
     color: white;
 }
 
 footer{
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  position: relative;
  background-color: #3d3d3d;
  border-radius: 0px;
  width: 100%;
  height: 515px;}
  
 #certified_image{
 display: block;
 margin-left: auto;
 margin-right: auto;
 margin-top: -170px;
 margin-bottom: -160px;
 height: 850px;
 width:  650px;}
 
 
 #footer-leaf{
 display: block;
 position: absolute;
 height: 100%;
 float: left;}
 
 .social_button{
 display: block;
 margin-left: auto;
 margin-right: auto;}

.disclaimer-button:hover{
background-color:#c1d72e;
color: white; }

.social-buttons{
background-color: #3d3d3d;
border-style: solid;
border-radius: 20px;
color: #d9d9d9;
height: 40px;
width: 40px;
margin-left: 15px;}

.social-buttons:hover{
background-color: #c1d72e;
color: white;}

#facebook-button{
margin-bottom: 20px;
margin-top: -20px;
margin-left: 10px;}

.modal-header{
background-color:#3d3d3d;
border-bottom-color: #3d3d3d;
}

.modal-body{
background-color: #3d3d3d;
border-color: #3d3d3d;
padding-bottom: 1px;
}
.modal-title{
font-family: Roboto;
color: white;
text-align: center;}

.modal-text{
font-family: Roboto;
color: white;
text-align: left;
}
.modal-footer{
background-color:#3d3d3d;
border-top-color: #3d3d3d;
margin-top: 1px;
}
.modal-content{
position: relative;
background-color: #3d3d3d;
z-index: +2;
}
.modal-dialog{
background-color: #3d3d3d;
}
.btn{
background-color: #3d3d3d;
color: white;}

.btn:hover{
background-color: #c1d72e;
color: white;}


#category1{
    margin-bottom:20px;
    margin-top:10px;
}

#category11{
    margin-bottom: 20px;
    margin-top: 10px;
}

#winner-text{
    text-align: center;
    font-weight: bold;
    color: #3d3d3d;
    text-shadow: none;
}

#winner-modal{
    background-color: #c1d72e;
    border-color: #c1d72e;
}

.winner-input{
    display: block;
    color: #3d3d3d;
    margin-left: 43%;
    margin-right: 50%;
}

#input-text{
    color: black;
}

#winner-button{
    display: block;
    float: center;
    color: #3d3d3d;
    border-color: #3d3d3d;
    border-style: solid;
    border-radius: 15px;
    background-color: #c1d72e;
    margin-left: 60%;
}

#winner-button:hover{
background-color: white;
border-color: white;
}

#loser-text{
    text-align: center;
    font-weight: bold;
}

#loser-modal{
    background-color: #ef2e31;
    border-color: #ef2e31;
}

#loserbutton1{
    border-color: white;
    border-radius: 15px;
    border-style: solid;
    background-color: #ef2e31;
    color: white;
    float: left;
    display: block;
    margin-left: 25%;
}

#loserbutton1:hover{
background-color: white;
color: #ef2e31;
}

#loserbutton2{
    border-color: white;
    border-radius: 15px;
    border-style: solid;
    background-color: #ef2e31;
    color: white;
    float: right;
    display: block;
    margin-right: 25%
}

#loserbutton2:hover{
background-color: white;
color: #ef2e31;
}

#winner-title{
color: #3d3d3d;
}
</style>
<head>

<title>SollePro Certification</title>
<div>
  <ul id="top_border">
  <a href="https://www.sollenaturals.com/home.php">
  <img id="img_logo" src="/solleprocertified/images/logo.jpg" alt="Solle Naturals Logo" height="40px" width="120px"> </a>
  <li> <a href="/solleprocertified/quiz-es.php">En Español</a></li>
  <li> <a href="/solleprocertified/presenters.html">Presenters</a></li>
  <li> <a href="/solleprocertified/quiz.php">Assessment</a></li>
  <li> <a href="https://www.sollenaturals.com/solleprocertified/mentorcert.html" >Mentor Training</a></li>
  <li> <a href="https://www.sollenaturals.com/solleprocertified/productcert.php">Product Training</a></li>
  <li> <a href="https://www.sollenaturals.com/solleprocertified/index.html">Home</a></li>
  </ul>
  </div>
<div> <ul id="mid_border">
</ul>
</head> <br><br><br>
<body>
    <?php
        if(!empty($errormessage))
        {
            echo("<p>There was an error with your form:</p>\n");
            echo("<ul>" . $errormessage . "</ul>\n");
        }
        ?>
        
<div class="bannerbar"> <p class="question2">1. Name a tool you can use to share Solle with someone</p>        
<ul class="answers2">            
<input type="radio" name="q1" value="a" id="q1a"><label id="altlabel" for="q1a">Videos</label><br/>          
<input type="radio" name="q1" value="b" id="q1b"><label id="altlabel" for="q1b">Webinars</label><br/>            
<input type="radio" name="q1" value="c" id="q1c"><label id="altlabel" for="q1c">Happy Hours</label><br/>            
<input type="radio" name="q1" value="d" id="q1d"><label id="altlabel"for="q1d">All of the above</label><br/>       
</ul> </div>     


<div class= "bannerbar2"><p class="question">2. How much CV does the Specialist Success Kit have?</p>        

<ul class="answers">            
<input type="radio" name="q2" value="a" id="q2a"><label for="q2a">100</label><br/>           
<input type="radio" name="q2" value="b" id="q2b"><label for="q2b">75</label><br/>            
<input type="radio" name="q2" value="c" id="q2c"><label for="q2c">50</label><br/>           
<input type="radio" name="q2" value="d" id="q2d"><label for="q2d">25</label><br/>       
</ul> </div>       

<div class="bannerbar"><p class="question2">3. Which of these ingredients can be found in Solle Vital?</p>        

<ul class="answers2">            
<input type="radio" name="q3" value="a" id="q3a"><label id="altlabel" for="q3a">Nettle</label><br/>            
<input type="radio" name="q3" value="b" id="q3b"><label id="altlabel" for="q3b">Gingko Biloba</label><br/>            
<input type="radio" name="q3" value="c" id="q3c"><label id="altlabel" for="q3c">Yerba Mate</label><br/>           
<input type="radio" name="q3" value="d" id="q3d"><label id="altlabel" for="q3d">Ginseng</label><br/>       
</ul> </div>       

<div class="bannerbar2"><p class="question">4. What percentage in SolleQuick bonus does a mentor receive when signing up a Specialist?</p>        

<ul class="answers">           
<input type="radio" name="q4" value="a" id="q4a"><label for="q4a">30%</label><br/>            
<input type="radio" name="q4" value="b" id="q4b"><label for="q4b">40%</label><br/>            
<input type="radio" name="q4" value="c" id="q4c"><label for="q4c">45%</label><br/>            
<input type="radio" name="q4" value="d" id="q4d"><label for="q4d">10%</label><br/>        
</ul> </div>

<div class="bannerbar"><p class="question2">5. At what rank do you start earning a Mentor Match bonus?</p>        
<ul class="answers2">            
<input type="radio" name="q5" value="a" id="q5a"><label id="altlabel" for="q5a">3k</label><br/>            
<input type="radio" name="q5" value="b" id="q5b"><label id="altlabel" for="q5b">5k</label><br/>            
<input type="radio" name="q5" value="c" id="q5c"><label id="altlabel" for="q5c">10k</label><br/>           
<input type="radio" name="q5" value="d" id="q5d"><label id="altlabel" for="q5d">15k</label><br/>        
</ul> </div> 

<div class="bannerbar2"><p class="question">6. Which of these statements is true about SolleFlexAC?</p>        
<ul class="answers">            
<input type="radio" name="q6" value="a" id="q6a"><label for="q6a">Contains animal products</label><br/>            
<input type="radio" name="q6" value="b" id="q6b"><label for="q6b">Contains menthol</label><br/>            
<input type="radio" name="q6" value="c" id="q6c"><label for="q6c">The results are slow and moderate</label><br/>            
<input type="radio" name="q6" value="d" id="q6d"><label for="q6d">Can be used as a carrier for essential oils</label><br/>        
</ul> </div>     

<div class="bannerbar"><p class="question2">7. One of the most nutritious herbs in CinnaMate is recommended by the Argentine government to <br>
supplement children's nutrition. This herb is:</p>        

<ul class="answers2">            
<input type="radio" name="q7" value="a" id="q7a"><label id="altlabel" for="q7a">Nopal leaf</label><br/>            
<input type="radio" name="q7" value="b" id="q7b"><label id="altlabel" for="q7b">Thyme</label><br/>            
<input type="radio" name="q7" value="c" id="q7c"><label id="altlabel" for="q7c">Banaba leaf</label><br/>            
<input type="radio" name="q7" value="d" id="q7d"><label id="altlabel" for="q7d">Yerba Mate</label><br/>        
</ul> </div>     

<div class="bannerbar2"><p class="question">8. The webinars and product information sheets can be found where?</p>        

<ul class="answers">            
<input type="radio" name="q8" value="a" id="q8a"><label for="q8a">The dashboard</label><br/>            
<input type="radio" name="q8" value="b" id="q8b"><label for="q8b">The document library</label><br/>            
<input type="radio" name="q8" value="c" id="q8c"><label for="q8c">Instagram</label><br/>            
<input type="radio" name="q8" value="d" id="q8d"><label for="q8d">YouTube</label><br/>       
</ul> </div>   

<div class="bannerbar"><p class="question2">9. The Maca in SolleExcell and SolleMaca XD has been shown to improve:</p>        

<ul class="answers2">            
<input type="radio" name="q9" value="a" id="q9a"><label id="altlabel" for="q9a">Thyroid levels</label><br/>            
<input type="radio" name="q9" value="b" id="q9b"><label id="altlabel" for="q9b">Mood</label><br/>            
<input type="radio" name="q9" value="c" id="q9c"><label id="altlabel" for="q9c">Bone Density</label><br/>            
<input type="radio" name="q9" value="d" id="q9d"><label id="altlabel" for="q9d">All of the above</label><br/>        
</ul> </div>      

<div class="bannerbar2"><p class="question">10. The protein in Solle Complete is:</p>        

<ul class="answers">           
<input type="radio" name="q10" value="a" id="q10a"><label for="q10a">Denatured whey protein from grass-fed cows</label><br/>            
<input type="radio" name="q10" value="b" id="q10b"><label for="q10b">Top quality amino acids derived synthetically</label><br/>            
<input type="radio" name="q10" value="c" id="q10c"><label for="q10c">Digestion-friendly pea proteins</label><br/>            
<input type="radio" name="q10" value="d" id="q10d"><label for="q10d">Derived primarily from soy</label><br/>        
</ul> </div>

<div class="bannerbar"><p class="question2">11. What is the name of Solle's 10 point quality guarantee?</p>

<ul class="answers2">
<input type="radio" name="q11" value="a" id="q11a"><label id="altlabel" for="q11a">SolleSafe</label><br/>
<input type="radio" name="q11" value="b" id="q11b"><label id="altlabel" for="q11b">SolleAssure</label><br/>
<input type="radio" name="q11" value="c" id="q11c"><label id="altlabel" for="q11c">SolleCertain</label><br/>
<input type="radio" name="q11" value="d" id="q11d"><label id="altlabel" for="q11d">SolleSecure</label><br/>
</ul></div>

<div class="bannerbar2"><p class="question">12. Which of the following statements pertaining to Solle's Mind/Body philosophy is true?</p>

<ul class="answers">
<input type="radio" name="q12" value="a" id="q12a"><label for="q12a">The mind and body are two distinct entities</label><br/>
<input type="radio" name="q12" value="b" id="q12b"><label for="q12b">Your mental condition rarely affects your physical health</label><br/>
<input type="radio" name="q12" value="c" id="q12c"><label for="q12c">Physical health is more important than emotional health</label><br/>
<input type="radio" name="q12" value="d" id="q12d"><label for="q12d">Both the mind and the body require good nutrition to function at their optimum levels</label><br/>
</ul></div>

<div class="bannerbar"><p class="question2">13. Which of the following is not a Solle product category?</p>

<ul class="answers2">
<input type="radio" name="q13" value="a" id="q13a"><label id="altlabel" for="q13a">Balance</label><br/>
<input type="radio" name="q13" value="b" id="q13b"><label id="altlabel" for="q13b">Strength</label><br/>
<input type="radio" name="q13" value="c" id="q13c"><label id="altlabel" for="q13c">Lift</label><br/>
<input type="radio" name="q13" value="d" id="q13d"><label id="altlabel" for="q13d">Clarify</label><br/>
</ul></div>

<div class="bannerbar2"><p class="question">14. Which of the following statements about adaptogens is false?</p>

<ul class="answers">
<input type="radio" name="q14" value="a" id="q14a"><label for="q14a">They must be used with supreme caution</label><br/>
<input type="radio" name="q14" value="b" id="q14b"><label for="q14b">They are a special class of herbs</label><br/>
<input type="radio" name="q14" value="c" id="q14c"><label for="q14c">They can help balance mood</label></br>
<input type="radio" name="q14" value="d" id="q14d"><label for="q14d">They adapt to the individual needs of the consumer</label></br>
</ul></div>

<div class="bannerbar"><p class="question2">15. Which of the following is not a benefit of Solle ReNue?</p>

<ul class="answers2">
<input type="radio" name="q15" value="a" id="q15a"><label id="altlabel" for="q15a">Helps you to relax</label><br/>
<input type="radio" name="q15" value="b" id="q15b"><label id="altlabel" for="q15b">Helps calm your mind</label><br/>
<input type="radio" name="q15" value="c" id="q15c"><label id="altlabel" for="q15c">Will give you X-ray vision</label><br/>
<input type="radio" name="q15" value="d" id="q15d"><label id="altlabel" for="q15d">Rejuvenates and rebuilds at cellular level</label><br/>
</ul></div>

<br/>
<div id="results">            
Submit  
</div>                

<div id="category1">            
     <!-- Modal Content -->
 	
 	<div class="modal-content" id="loser-modal">
 		<div class="modal-header" id="loser-modal">
 		<button type="button" class="close" data-dismiss="modal">&times;</button>
 		<h4 class="modal-title">Oops! Solle Close!</h4>
 		</div>
 		<div class="modal-body" id="loser-modal">
 		<p class="modal-text" id="loser-text">You got questions incorrect. Refresh the page to try again<br>
         Or click one of these buttons to return to the training modules <br></p>
 		</div>
 		<div class="modal-footer" id="loser-modal" > 
         <button id="loserbutton1" onclick="window.location.href='https://www.sollenaturals.com/solleprocertified/productcert.php'">Product Training</button> 
         <button id="loserbutton2" onclick="window.location.href='https://www.sollenaturals.com/solleprocertified/mentorcert.html'">Mentor Training</button>
 		</div>
 		</div>
 	</div> 

<div id="category11">
 	<!-- Modal Content -->
 	
 	<div class="modal-content" id="winner-modal">
 		<div class="modal-header" id="winner-modal">
 		<button type="button" class="close" data-dismiss="modal">&times;</button>
 		<h4 class="modal-title" id="winner-title">Congratulations!</h4>
 		</div>
 		<div class="modal-body" id="winner-modal">
 		<p class="modal-text" id="winner-text">You scored 100%! You are now SollePro Certified!<br>
         Please enter your name and email address to receive your SollePro Certificate! <br></p>
         <form name="myForm" action="quiz.php" method="post" class="winner-input"> Name: <input id="input-text" type="text" name="formName" value="<?=$varName;?>" required ><br>
         Email: <input id="input-text" type="email" name="formEmail" value="<?=$varEmail;?>" required><br><br>
         <input id="winner-button" type="submit" name="formSubmit" value="Submit">
         </form>
 		</div>
 		<div class="modal-footer" id="winner-modal">
 		</div>
 	</div>
 </div>
</div>



<footer class="container-fluid"><img id="footer-leaf" src="/solleprocertified/images/footer_leaf.png"> 
 <div class="footer_text"> <div id="center-buttons"><button class="fa fa-facebook fa-2x social-buttons" id="facebook-button"  onclick="window.open('https://www.facebook.com/SolleNaturals/','_blank');";></button>
 <button class="fa fa-instagram fa-2x social-buttons" onclick="window.open('https://www.instagram.com/sollenaturalstm/','_blank');";> </button> 
 <button class="fa fa-twitter fa-2x social-buttons" onclick="window.open('https://twitter.com/sollenaturals1','_blank');";></button> 
 <button class="fa fa-youtube fa-2x social-buttons" onclick="window.open('https://www.youtube.com/user/sollenaturals','_blank');";> </button> </div>
 <p align="center"> 260 S. 2500 W.<br> Suite 102, Pleasant Grove, Utah 84062 <br> <br>
 Toll Free #:888-787-0665 <br> Email: info@sollenaturals.com </p> <br> <br>
 <img id="footer-logo" src="/solleprocertified/images/solle_logo_footer.png"> <br> <br> 
 <p align="center" id="copyright-text"> &copy Copyright Solle Naturals 2017. All Rights Reserved.</p> <br> <br>
 
 <!-- Modal Bootstrap testing -->
 
 <button class="disclaimer-button" data-toggle="modal" data-target="#mymodal">Legal Disclaimer</button>
 <div id="mymodal" class="modal fade" role"dialog">
 	<div class="modal-dialog">
 	
 	<!-- Modal Content -->
 	
 	<div class="modal-content">
 		<div class="modal-header">
 		<button type="button" class="close" data-dismiss="modal">&times;</button>
 		<h4 class="modal-title">Legal Disclaimer</h4>
 		</div>
 		<div class="modal-body">
 		<p class="modal-text">The content of this website is intended for education purposes only. <br>
         It is not intended to be a substitute for professional healthcare advice, diagnosis or treatment. <br>
         We encourage you to consult your healthcare professional if you have concerns about your physical or emotional well-being.</p>
 		</div>
 		<div class="modal-footer">
 		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 		</div>
 	</div>
 </div>
</div>
</footer>
</html>